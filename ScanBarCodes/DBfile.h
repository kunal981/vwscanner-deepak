//
//  DBfile.h
//  Fruits
//
//  Created by brst on 6/18/14.
//  Copyright (c) 2014 sh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBfile : NSObject
{
   
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong)NSString *barcodeUrl;
@property (nonatomic, strong)NSString  *barcode_id;

@end
