//
//  DBManager.h
//  FinalNavitas
//
//  Created by brst on 6/10/14.
//  Copyright (c) 2014 sh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager2 : NSObject
{
    NSString *databasePath;
}
+(DBManager2*)getSharedInstance;
-(BOOL)createDB;
-(BOOL) saveData:(NSString*)name url:(NSString*)url note:(NSString*)note;
-(BOOL)deleteRecord:(NSString *)barcode_id;
-(BOOL)updateTable:(NSString *)note index:(NSString*)rowID;
-(BOOL)deleteAll;
- (NSMutableArray *) showAll;
@end
