//
//  historyViewController.h
//  VWScanner
//
//  Created by brst on 3/24/15.
//  Copyright (c) 2015 Infragistics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface historyViewController : UIViewController<UITextFieldDelegate>
{
    NSMutableArray *barcodeArr;
    UILabel  *lblText;
    UILabel *lblDetailText;
    UILabel *lblNoteText;
    
    UIView *blurView;
    
    UILabel *lblBarNumber;
    
    UIImageView *imgViewBar;
    
    UITextView *viewNotes;
    UITextField *txtNotes;
    UIButton *btnDone;
    
    
    NSString *newNotes;
    NSString *selectedIndex;
    NSString *deletedID;
    NSInteger cellHeight;
}
@property (nonatomic,strong)NSMutableArray *barcodeArr;
@end
