
#import <UIKit/UIKit.h>
#import "DBManager2.h"
#import "DBfile.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface FirstViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate>
{
    AVAudioPlayer *player;
    UIView *blurView;
    UIBlurEffect *blurEffect;
    UIVisualEffectView *blurEffectView;
    UILabel *lblBarNumber;
    
    UIImageView *imgViewBar;
    
    UITextView *viewNotes;
    UITextField *txtNotes;
    UIButton *btnDone;
    NSString *detString;
    NSArray *metaArray;
    
}
@property (strong, nonatomic)AVAudioPlayer *player;
@end