//
//  historyViewController.m
//  VWScanner
//
//  Created by brst on 3/24/15.
//  Copyright (c) 2015 Infragistics. All rights reserved.
//

#import "historyViewController.h"
#import "DBfile.h"
#import "DBManager2.h"
#import <MessageUI/MessageUI.h>

@interface historyViewController ()<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
    UITableView *table;
    UILabel *historyLabl;
}

@end

@implementation historyViewController
@synthesize barcodeArr;


-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor blackColor]];
    self.view = view;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self toplayerView];
    table=[[UITableView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
    [table setSeparatorColor:[UIColor grayColor]];
    table.backgroundColor=[UIColor clearColor];
    table.delegate=self;
    table.dataSource=self;
    table.allowsMultipleSelectionDuringEditing=NO;
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:table];
    
    cellHeight = 70;
    
}
-(void)viewWillAppear:(BOOL)animated
{
   
    if(barcodeArr.count==0)
    {
        table.hidden=YES;
    }
    else
    {
        table.hidden=NO;
    }
    
    historyLabl.text=@"History";
    if(barcodeArr.count>0)
    {
        historyLabl.text=[NSString stringWithFormat:@"History(%lu)",(unsigned long)barcodeArr.count];
        [table reloadData];
    }

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return barcodeArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        CGFloat heigth = 18;
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
        
        lblText = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-30,heigth)];
        lblText.textAlignment = NSTextAlignmentLeft;
        lblText.lineBreakMode = NSLineBreakByWordWrapping;
        lblText.font = [UIFont systemFontOfSize:15];
        lblText.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lblText];
        
        
        
        lblDetailText = [[UILabel alloc]initWithFrame:CGRectMake(15, lblText.frame.origin.y+lblText.frame.size.height, self.view.frame.size.width-30, heigth)];
        lblDetailText.textAlignment = NSTextAlignmentLeft;
        lblDetailText.font = [UIFont systemFontOfSize:14];
        lblDetailText.textColor = [UIColor grayColor];
        lblDetailText.lineBreakMode = NSLineBreakByWordWrapping;
        [cell.contentView addSubview:lblDetailText];
        
        lblNoteText = [[UILabel alloc]initWithFrame:CGRectMake(15, lblDetailText.frame.origin.y+lblDetailText.frame.size.height, self.view.frame.size.width-5, heigth)];
        lblNoteText.textAlignment = NSTextAlignmentLeft;
        lblNoteText.font = [UIFont systemFontOfSize:13];
        lblNoteText.textColor = [UIColor grayColor];
        lblNoteText.lineBreakMode = NSLineBreakByWordWrapping;
        [cell.contentView addSubview:lblNoteText];
        
        
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor grayColor];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
    
     DBfile *dbObj=[barcodeArr objectAtIndex:indexPath.row];
    NSLog(@"Lable Length : %lu",(unsigned long)dbObj.barcodeUrl.length);

     lblText.text=dbObj.barcodeUrl;
     lblDetailText.text = dbObj.barcodeUrl;
    
//    if (dbObj.barcodeUrl.length > 50) {
//        lblText.numberOfLines = 2;
//        lblText.frame = CGRectMake(15, 10, self.view.frame.size.width,50);
//        
////        lblDetailText.frame = CGRectMake(15, lblText.frame.origin.y+lblText.frame.size.height, self.view.frame.size.width, 50);
////        lblDetailText.numberOfLines = 2;
//        cellHeight = 90;
//        
//    }
    
    NSLog(@"%lu",(unsigned long)dbObj.note.length);
    if (dbObj.note.length>= 40 && dbObj.note.length <80 ) {
        lblNoteText.numberOfLines = 2;
        lblNoteText.frame = CGRectMake(15, lblDetailText.frame.origin.y+lblDetailText.frame.size.height, self.view.frame.size.width, 50);
        cellHeight = 90;
    }
    else if (dbObj.note.length >=80 && dbObj.note.length < 120 ){
        lblNoteText.numberOfLines = 3;
        lblNoteText.frame = CGRectMake(15, lblDetailText.frame.origin.y+lblDetailText.frame.size.height, self.view.frame.size.width, 70);
        cellHeight = 110;
    }
    else if (dbObj.note.length >=120  ){
        lblNoteText.numberOfLines = 4;
        lblNoteText.frame = CGRectMake(15, lblDetailText.frame.origin.y+lblDetailText.frame.size.height, self.view.frame.size.width, 90);
        cellHeight = 130;
    }
    else {
        cellHeight = 70;
    }
  
    if ([dbObj.note isEqualToString:@""]) {
        lblNoteText.text = @"";
    }
    else {
     lblNoteText.text = [NSString stringWithFormat:@"Note:%@",dbObj.note];
    }
     dbObj=nil;
    

    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    
    DBfile *dbObj=[barcodeArr objectAtIndex:indexPath.row];
    lblText.text=dbObj.barcodeUrl;
    lblDetailText.text = dbObj.barcodeUrl;
    deletedID = dbObj.barcode_id;
    NSLog(@"note %@",deletedID);
    if ([dbObj.note isEqualToString:@""]) {
        
    }
    else {
   
        CGFloat heightFactor = 0.416;
        
        NSLog(@"height: %f", (150.0/320)*self.view.frame.size.width);
        
        blurView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.view.frame.size.height-200)-(heightFactor * self.view.frame.size.height), self.view.frame.size.width, heightFactor * self.view.frame.size.height)];
        
        blurView.backgroundColor = [UIColor colorWithRed:(162/255.0f) green:(162/255.0f) blue:(162/255.0f) alpha:0.1f];
        
        imgViewBar  = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, (120.0/320)*self.view.frame.size.width, (120.0/320)*self.view.frame.size.width)];
        //imgViewBar.backgroundColor = [UIColor redColor];
        imgViewBar.image = [UIImage imageNamed:@"VW_blue_Globe_with_ring.png"];
        
        [blurView addSubview:imgViewBar];
        
        lblBarNumber = [[UILabel alloc]initWithFrame:CGRectMake(imgViewBar.frame.origin.x+imgViewBar.frame.size.width+1, imgViewBar.frame.size.height/2, (150.0/320)*self.view.frame.size.width, (50.0/568) * self.view.frame.size.height)];
        lblBarNumber.text = dbObj.barcodeUrl ;
        lblBarNumber.lineBreakMode = NSLineBreakByWordWrapping;
        lblBarNumber.numberOfLines =3;
        lblBarNumber.textAlignment = NSTextAlignmentCenter;
        lblBarNumber.textColor = [UIColor redColor];
        lblBarNumber.font = [UIFont boldSystemFontOfSize:17];
        
        
        [blurView addSubview:lblBarNumber];
        
        
        
        txtNotes = [[UITextField alloc]initWithFrame:CGRectMake(5, imgViewBar.frame.origin.y + imgViewBar.frame.size.height+10, self.view.frame.size.width-10, (37.0/568) * self.view.frame.size.height)];
        txtNotes.returnKeyType = UIReturnKeyDone;
        txtNotes.text = dbObj.note;
       
        
        txtNotes.delegate = self;
        txtNotes.backgroundColor = [UIColor blackColor];
        txtNotes.textColor = [UIColor whiteColor];
        txtNotes.layer.borderWidth = 1.0;
        txtNotes.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [blurView addSubview:txtNotes];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        txtNotes.leftView = paddingView;
        txtNotes.leftViewMode = UITextFieldViewModeAlways;
        
        btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        btnDone.frame = CGRectMake(txtNotes.frame.origin.x + txtNotes.frame.size.width+2, imgViewBar.frame.origin.y + imgViewBar.frame.size.height+10, 70, (37.0/568) * self.view.frame.size.height);
        btnDone.backgroundColor = [UIColor blackColor];
        [btnDone setTitle:@"Done" forState:UIControlStateNormal];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[btnDone addTarget:self action:@selector(donePressed)  forControlEvents:UIControlEventTouchUpInside];
        btnDone.layer.borderColor = [UIColor lightGrayColor].CGColor;
        btnDone.layer.borderWidth = 1.0;
       // [blurView addSubview:btnDone];
        
        [self.view addSubview:blurView];
        
    }
    
}

-(void)donePressedUpdate{
    
   
    [txtNotes resignFirstResponder];
    
}

-(void)toplayerView
{
    UIView *blueLine=[[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 1)];
    blueLine.backgroundColor=[UIColor colorWithRed:0.f green:(153/255.0f) blue:(240/255.0f) alpha:1];
    [self.view addSubview:blueLine];
    
    /*UIImageView *iconImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 35, 35)];
    //iconImage.layer.cornerRadius=iconImage.frame.size.width/2;
    iconImage.image=[UIImage imageNamed:@"VW_blue_Globe_with_ring.png"];
    [self.view addSubview:iconImage];*/
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 25, 25)];
    [backButton setImage:[UIImage imageNamed:@"backSymbol.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UIButton *backButtonSpace=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];

    [backButtonSpace addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonSpace];
    
    
    historyLabl=[[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 35)];
    
    historyLabl.textAlignment=NSTextAlignmentCenter;
    historyLabl.textColor=[UIColor whiteColor];
    [self.view addSubview:historyLabl];
    
    UIButton *deleteBtn=[[UIButton alloc]init];
    deleteBtn.frame=CGRectMake(self.view.frame.size.width-50, 20, 35, 35);
    [deleteBtn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteAllHistory) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:deleteBtn];
    
    UIButton *shareBtn=[[UIButton alloc]init];
    shareBtn.frame=CGRectMake(self.view.frame.size.width-100, 20, 35, 35);
     //shareBtn.frame=CGRectMake(self.view.frame.size.width-50, 20, 35, 35);
    [shareBtn setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareBtn];
    
}
-(void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DBfile *obj=[barcodeArr objectAtIndex:indexPath.row];
        [[DBManager2 getSharedInstance]deleteRecord:obj.barcode_id];
        [barcodeArr removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        if(barcodeArr.count>0)
            historyLabl.text=[NSString stringWithFormat:@"History(%lu)",(unsigned long)barcodeArr.count];
        else
        {
            historyLabl.text=@"History";
            tableView.hidden=YES;
        }
    }
    
}
-(void)createCsvFile
{
    BOOL success = NO;
    
    //Creating Excel file from database recod stored in array
    
    NSMutableArray *barcodeArr1=[[NSMutableArray alloc]init];
    [barcodeArr1 addObject:@"Sr. no,Name,URL"];
    for(int i=0;i<barcodeArr.count;i++)
    {
        DBfile *obj=[barcodeArr objectAtIndex:i];
        [barcodeArr1 addObject:[NSString stringWithFormat:@"\n%d,%@,%@",(i+1),obj.name,obj.barcodeUrl]];
        obj=nil;
    }
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filename=[NSString stringWithFormat:@"barcode.csv"];
    NSString *filePathLib = [NSString stringWithFormat:@"%@",[docDir stringByAppendingPathComponent:filename]];
    NSLog(@"filepath=%@",filePathLib);
    
    [[barcodeArr1 componentsJoinedByString:@","] writeToFile:filePathLib atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    //Sending excel file via mail
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@" File from VWScanner App"];
        [mail setToRecipients:@[@"testingEmail@example.com"]];
        
        NSData* databaseRecord = [NSData dataWithContentsOfFile: filePathLib];
        [mail addAttachmentData:databaseRecord mimeType:@"application/octet-stream" fileName:filename];
        NSString* emailBody = @"Attached is the Database Record of barcode Scan from VWScanner App.";
        [mail setMessageBody:emailBody isHTML:YES];
        [self presentViewController:mail animated:YES completion:NULL];
        success = YES;
    }
    else
    {
        NSLog(@"This device cannot send email");
        
    }

    if (!success) {
        UIAlertView* warning = [[UIAlertView alloc] initWithTitle: @"Sorry!"
                                                          message: @"Unable to send Mail!"
                                                         delegate: nil
                                                cancelButtonTitle: @"Ok"
                                                otherButtonTitles: nil];
        [warning show];
        
    }

}

-(void)shareBtnPressed
{
    if(barcodeArr.count>0)
    {
        [self createCsvFile];
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //BOOL success = NO;
    NSString *msg;
    switch (result) {
        case MFMailComposeResultSent:
            msg=@"Your email is successfully sent.";
            break;
        case MFMailComposeResultSaved:
            msg=@"Your email is successfully saved to draft";
            break;
        case MFMailComposeResultCancelled:
            msg=@"You cancelled sending this email.";
            break;
        case MFMailComposeResultFailed:
            msg=@"An error occurred when trying to compose this email";
            break;
        default:
            msg=@"An error occurred when trying to compose this email";
            break;
    }
    NSLog(@"msg=%@",msg);
    [self dismissViewControllerAnimated:YES completion:NULL];
    UIAlertView* warning = [[UIAlertView alloc] initWithTitle: nil
                                                      message: msg
                                                     delegate: nil
                                            cancelButtonTitle: @"OK"
                                            otherButtonTitles: nil];
    [warning show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void )deleteAllHistory
{
    
    UIAlertView* warning = [[UIAlertView alloc] initWithTitle: nil
                                                      message: @"Are you sure you want to delete history?"
                                                     delegate: self
                                            cancelButtonTitle: @"Cancel"
                                            otherButtonTitles: @"OK",nil];
    [warning show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        NSLog(@"Delate All=%d", [[DBManager2 getSharedInstance]deleteAll]);
        [barcodeArr removeAllObjects];
        [table reloadData];
        historyLabl.text=@"History";
       
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.text.length==0) {
        UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Notes cannot be empty " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        
        [textField resignFirstResponder];
        newNotes = textField.text;
        
        NSLog(@"new notes %@",newNotes);
        
        BOOL success = [[DBManager2 getSharedInstance]updateTable:newNotes index:deletedID];
        NSLog(@"inserted %d",success);
        [blurView removeFromSuperview];
        [barcodeArr removeAllObjects];
        barcodeArr =  [[DBManager2 getSharedInstance]showAll];
        [table reloadData];
    }

    
    return  YES;
}

@end
