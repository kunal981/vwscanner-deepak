//
//  igViewController.m
//  ScanBarCodes
//
//  Created by Torrey Betts on 10/10/13.
//  Copyright (c) 2013 Infragistics. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "FirstViewController.h"
#import "historyViewController.h"
#import "SettingViewController.h"
#import "WebviewController.h"


@interface FirstViewController() <AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;

    UIView *_highlightView;
    UILabel *_label;
    historyViewController *historyObj;
    SettingViewController *settingObj;
    NSMutableArray *barcodeArr;
    NSString *oldString;
    UIWebView *webView;
    WebviewController *webObj;
    CGRect frame;
    NSString *checkScanStatus,*checkNote;
    UIAlertView *alert;
}
@end

@implementation FirstViewController
@synthesize player;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self playSound];
   
    barcodeArr=[[NSMutableArray alloc]init];
   
   // self.view.backgroundColor=[UIColor colorWithWhite:0.15 alpha:0.65];
    self.view.backgroundColor=[UIColor colorWithRed:(162/255.0f) green:(162/255.0f) blue:(162/255.0f) alpha:0.1f];
    
    _highlightView = [[UIView alloc] init];
   _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    //_highlightView.frame=CGRectMake(60, 150, self.view.frame.size.width-120, self.view.frame.size.height-300);
    _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
    _highlightView.layer.borderWidth = 3;
    frame=_highlightView.frame;
    [self.view addSubview:_highlightView];
    

    _label = [[UILabel alloc] init];
    _label.frame = CGRectMake(0, self.view.bounds.size.height - 40, self.view.bounds.size.width, 40);
    _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
    _label.textColor = [UIColor whiteColor];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.text = @"Place a Barcode to scan it.";
    [self.view addSubview:_label];

    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;

    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }

    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    

    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];

    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame=self.view.bounds;
    
    //_prevLayer.frame = CGRectMake(60, 150, self.view.frame.size.width-120, self.view.frame.size.height-300);
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
  
    [self.view.layer addSublayer:_prevLayer];

   
    alert=[[UIAlertView alloc]initWithTitle:@"VWScanner" message:@"Sorry,Barcode contents may be invalid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate=self;

    [self.view bringSubviewToFront:_highlightView];
    [self.view bringSubviewToFront:_label];
    [self toplayerView];
   

}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    oldString=@"";
    _highlightView.frame=frame;
    _label.text = @"Place a Barcode to scan it.";

     [_session startRunning];
    [player stop];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [player stop];
    [blurView removeFromSuperview];
}
-(void)toplayerView
{
    UIView *blueLine=[[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 1)];
    blueLine.backgroundColor=[UIColor colorWithRed:0.f green:(153/255.0f) blue:(240/255.0f) alpha:1];
    [self.view addSubview:blueLine];
    
    UIImageView *iconImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 35, 35)];
    //iconImage.layer.cornerRadius=iconImage.frame.size.width/2;
    iconImage.image=[UIImage imageNamed:@"VW_blue_Globe_with_ring.png"];
    [self.view addSubview:iconImage];
    
    UILabel *VWScannerLabl=[[UILabel alloc]initWithFrame:CGRectMake(50, 20, 150, 35)];
    VWScannerLabl.text=@"VW Scanner";
    VWScannerLabl.textColor=[UIColor whiteColor];
    [self.view addSubview:VWScannerLabl];
    
    UIButton *settingBtn=[[UIButton alloc]init];
    settingBtn.frame=CGRectMake(self.view.frame.size.width-50, 20, 35, 35);
    [settingBtn setImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(settingBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBtn];
    
   
    UIButton *historyBtn=[[UIButton alloc]init];
    historyBtn.frame=CGRectMake(self.view.frame.size.width-100, 20, 35, 35);
    [historyBtn setImage:[UIImage imageNamed:@"history.png"] forState:UIControlStateNormal];
    [historyBtn addTarget:self action:@selector(historyBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:historyBtn];

}
-(void)historyBtnPressed
{
    if(!historyObj)
    {
         historyObj=[[historyViewController alloc]init];
    }
    NSLog(@"showAll=%@",[[DBManager2 getSharedInstance]showAll]);
    historyObj.barcodeArr=[[DBManager2 getSharedInstance]showAll];
   
    [self.navigationController pushViewController:historyObj animated:YES];
}

-(void)settingBtnPressed
{
    if(!settingObj)
    {
        settingObj=[[SettingViewController alloc]init];
    }
    
    [self.navigationController pushViewController:settingObj animated:YES];
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSLog(@"%@",metadataObjects);
  
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];

    for (AVMetadataObject *metadata in metadataObjects) {
        NSLog(@"%@",metadata);
        
        
        for (NSString *type in barCodeTypes){
            if ([metadata.type isEqualToString:type])
            {
                NSLog(@"%@",type);
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                CMTime time = [(AVMetadataMachineReadableCodeObject*)metadata time];
                
                Float64 dur = CMTimeGetSeconds(time);
                Float64 durInMiliSec = 1000000000*dur;
                
               NSLog(@"seconds = %f", durInMiliSec);
                NSString *strTime  = [NSString stringWithFormat:@"%f",durInMiliSec];
                NSLog(@"%@",strTime);
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:([strTime doubleValue]/1000 )];
                NSDateFormatter *dtfrm = [[NSDateFormatter alloc] init];
                [dtfrm setDateFormat:@"MM/dd/yyyy"];
                NSString *Newdate = [dtfrm stringFromDate:date];
                NSLog(@"date=%@",Newdate);
                break;
            }
        }

        if (detectionString != nil)
        {
            _label.text = detectionString;
           // if(![oldString isEqualToString:detectionString])
            {
                 [player play];
               
                
                //[barcodeArr addObject:detectionString];
                oldString=detectionString;
                checkScanStatus=[[NSUserDefaults standardUserDefaults]valueForKey:@"autoScan"];
                checkNote = [[NSUserDefaults standardUserDefaults]valueForKey:@"Note"];
                //[player stop];
                if([checkScanStatus isEqualToString:@"OFF"] && [checkNote isEqualToString:@"OFF"])
                {
                   [_session stopRunning];
                   //detectionString= [self validateUrl:detectionString];
                    if(detectionString.length>0)
                    {
                        

                        if(!webObj)
                        {
                            webObj=[[WebviewController alloc]init];
                        }
                        webObj.url=detectionString;
                        webObj.metaDataObj=metadataObjects[0];
                        //[player stop];
                    
                        [self.navigationController pushViewController:webObj animated:YES];
                    }
                    
                }
                
                if ([checkNote isEqualToString:@"ON"]) {
                    [_session stopRunning];
                    //detectionString= [self validateUrl:detectionString];
                    if(detectionString.length>0)
                    {
                       
                        [_session stopRunning];
                        _highlightView.hidden = YES;
                        
                        CGFloat heightFactor = 0.416;
                        
                        NSLog(@"height: %f", (150.0/320)*self.view.frame.size.width);
                        
                        blurView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.view.frame.size.height-200)-(heightFactor * self.view.frame.size.height), self.view.frame.size.width, heightFactor * self.view.frame.size.height)];
                        
                        blurView.backgroundColor = [UIColor colorWithRed:(162/255.0f) green:(162/255.0f) blue:(162/255.0f) alpha:0.1f];
                        
                        imgViewBar  = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, (120.0/320)*self.view.frame.size.width, (120.0/320)*self.view.frame.size.width)];
                        //imgViewBar.backgroundColor = [UIColor redColor];
                        imgViewBar.image = [UIImage imageNamed:@"VW_blue_Globe_with_ring.png"];
                        
                        [blurView addSubview:imgViewBar];
                        
                        lblBarNumber = [[UILabel alloc]initWithFrame:CGRectMake(imgViewBar.frame.origin.x+imgViewBar.frame.size.width+1, imgViewBar.frame.size.height/2, (100.0/320)*self.view.frame.size.width, (50.0/568) * self.view.frame.size.height)];
                        lblBarNumber.text = _label.text;
                        lblBarNumber.lineBreakMode = NSLineBreakByWordWrapping;
                        lblBarNumber.numberOfLines =3;
                        lblBarNumber.textAlignment = NSTextAlignmentCenter;
                        lblBarNumber.textColor = [UIColor redColor];
                        lblBarNumber.font = [UIFont boldSystemFontOfSize:17];
                        
                        
                        [blurView addSubview:lblBarNumber];
                        
                        
                        
                        txtNotes = [[UITextField alloc]initWithFrame:CGRectMake(5, imgViewBar.frame.origin.y + imgViewBar.frame.size.height+10, 220, (37.0/568) * self.view.frame.size.height)];
                        txtNotes.placeholder = @"Enter Notes ";
                        if ([txtNotes respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                            UIColor *color = [UIColor whiteColor];
                            txtNotes.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Notes Here.." attributes:@{NSForegroundColorAttributeName: color}];
                        } else {
                            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
                            // TODO: Add fall-back code to set placeholder color.
                        }
                        
                        txtNotes.delegate = self;
                        txtNotes.backgroundColor = [UIColor blackColor];
                        txtNotes.textColor = [UIColor whiteColor];
                        txtNotes.layer.borderWidth = 1.0;
                        txtNotes.layer.borderColor = [UIColor lightGrayColor].CGColor;
                        [blurView addSubview:txtNotes];
                        
                        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
                        txtNotes.leftView = paddingView;
                        txtNotes.leftViewMode = UITextFieldViewModeAlways;
                        
                        btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnDone.frame = CGRectMake(txtNotes.frame.origin.x + txtNotes.frame.size.width+2, imgViewBar.frame.origin.y + imgViewBar.frame.size.height+10, 70, (37.0/568) * self.view.frame.size.height);
                        btnDone.backgroundColor = [UIColor blackColor];
                        [btnDone setTitle:@"Done" forState:UIControlStateNormal];
                        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [btnDone addTarget:self action:@selector(donePressed)  forControlEvents:UIControlEventTouchUpInside];
                        btnDone.layer.borderColor = [UIColor lightGrayColor].CGColor;
                        btnDone.layer.borderWidth = 1.0;
                        metaArray = metadataObjects;
                         detString = detectionString;
                        
                        [blurView addSubview:btnDone];
                        
                        [self.view addSubview:blurView];
                        
                       
                        
                       
                    }
                    
                }
                else if ([checkScanStatus isEqualToString:@"ON"] && [checkNote isEqualToString:@"OFF"]){
                    
                    BOOL success = [[DBManager2 getSharedInstance]saveData:@"Barcode" url:detectionString note:@""];
                     NSLog(@"inserted %d",success);
                    [_session stopRunning];
                    NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self
                                                                      selector: @selector(sessionStart) userInfo: nil repeats: YES];
                    
                }
             
            }
           
            break;
        }
        else
            _label.text=@"Place a Barcode to scan it.";
        
    }

    _highlightView.frame = highlightViewRect;
    //[player stop];
}



- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
//-(NSString *)validateUrl:(NSString *)strUrl
//{
//    NSURL *url;
//    url=[NSURL URLWithString:strUrl];
//    if ([url.scheme isEqualToString:@"http"]||[url.scheme isEqualToString:@"HTTP"]||[url.scheme isEqualToString:@"https"]||[url.scheme isEqualToString:@"HTTPS"])
//    {
//        url = [NSURL URLWithString:strUrl];
//       // NSLog(@"%@ is a valid URL", strUrl);
//        return strUrl;
//    }
//    else{
//        strUrl=[[NSUserDefaults standardUserDefaults]valueForKey:@"CustomURL"];
//        url=[NSURL URLWithString:strUrl];
//        if ([url.scheme isEqualToString:@"http"]||[url.scheme isEqualToString:@"HTTP"]||[url.scheme isEqualToString:@"https"]||[url.scheme isEqualToString:@"HTTPS"])
//        {
//           NSLog(@"%@ is a valid URL", url);
//            return strUrl;
//        }
//        [alert show];
//    }
//    return @"";
//}


- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NSLog(@"buttonIndex=%ld",(long)buttonIndex);
    if(buttonIndex==0)
    {
        oldString=@"";
        _highlightView.frame=frame;
      //  _label.text=@"";
        [_session startRunning];
    }
    
}

-(void)playSound
{
    
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/beep.mp3",[[NSBundle mainBundle] resourcePath]];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
  
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    [player setVolume:5.0];
  //  player.numberOfLoops = -1; //Infinite
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
       
    
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)donePressed{
   
    NSLog(@"%@",txtNotes.text);
    
    if (txtNotes.text.length ==0) {
        
        UIAlertView *alert1  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Notes cannot be empty " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert1 show];

        
    }else{
    BOOL success = [[DBManager2 getSharedInstance]saveData:@"Barcode" url:detString note:txtNotes.text];
    NSLog(@"inserted %d",success);
    if(!historyObj)
    {
        historyObj=[[historyViewController alloc]init];
    }
    
    historyObj.barcodeArr=[[DBManager2 getSharedInstance]showAll];

    
    [self.navigationController pushViewController:historyObj animated:YES];
    }
}

-(void)sessionStart{
    [_session startRunning];
}
@end