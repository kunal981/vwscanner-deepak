//
//  WebviewController.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "WebviewController.h"
#import "MBProgressHUD.h"


@implementation WebviewController
@synthesize metaDataObj;

- (void) viewDidLoad {
  [super viewDidLoad];
    alert=[[UIAlertView alloc]initWithTitle:@"VW Scanner" message:@"Sorry! Invalid URL"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate=self;
    
    [self toplayerView];
    [self load];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [webview removeFromSuperview];
}


-(void)toplayerView
{
    UIView *blueLine=[[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 1)];
    blueLine.backgroundColor=[UIColor colorWithRed:0.f green:(153/255.0f) blue:(240/255.0f) alpha:1];
    [self.view addSubview:blueLine];
    
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 25, 25)];
    [backButton setImage:[UIImage imageNamed:@"backSymbol.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UIButton *backButtonSpace=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    
    [backButtonSpace addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonSpace];

    
    UILabel *VWScannerLabl=[[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 35)];
    VWScannerLabl.text=@"VW Scanner";
    VWScannerLabl.textAlignment=NSTextAlignmentCenter;
    VWScannerLabl.textColor=[UIColor whiteColor];
    [self.view addSubview:VWScannerLabl];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
   isLoading = NO;
 
    
    webview=[[UIWebView alloc] initWithFrame:CGRectMake(0, 61, self.view.frame.size.width, self.view.frame.size.height)];
    webview.delegate=self;
    [self.view addSubview:webview];
    self.url= [self validateUrl2:self.url];
    
    if(self.url.length>0)
    //[webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    {
        NSURL *MyUrl = [NSURL URLWithString:[self.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"URL=%@ type=%@  value=%@",MyUrl,metaDataObj.type,[(AVMetadataMachineReadableCodeObject *)metaDataObj stringValue]);
        requestObj = [NSURLRequest requestWithURL:MyUrl];
        [webview loadRequest:requestObj];
    }
  
    
    else
        [alert show];
}

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor blackColor]];
    self.view = view;
    
}
-(void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) load {
    
       NSLog(@"URL=%@",self.url);
    
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self hideLoadingView];
    [alert show];
  //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.url]];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    if(!isLoading)
      [self showLoadingView];
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
  //isLoading = webview.loading;
    
    [self hideLoadingView];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if(!isDone)
    {
        if(!isLoading)
            [self showLoadingView];
        
        isDone=NO;
        con=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self startImmediately:YES];
        [con start];
        return NO;
    }
    
    
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if([challenge previousFailureCount]==0)
    {
        isDone=YES;
        NSURLCredential *credential=[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    isDone=YES;
    
    [webview loadRequest:requestObj];
    [con cancel];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideLoadingView];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
        [self.navigationController popViewControllerAnimated:NO];
    
}

#pragma mark - Loading View Method's
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    isLoading = YES;
    
}
-(void)hideLoadingView
{
    //isLoading = NO;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*-(NSString *)validateUrl:(NSString *)strUrl
{
    NSURL *url;
    url=[NSURL URLWithString:strUrl];
    if ([url.scheme isEqualToString:@"http"]||[url.scheme isEqualToString:@"HTTP"]||[url.scheme isEqualToString:@"https"]||[url.scheme isEqualToString:@"HTTPS"])
    {
        url = [NSURL URLWithString:strUrl];
        // NSLog(@"%@ is a valid URL", strUrl);
        return strUrl;
    }
    else{
        strUrl=[[NSUserDefaults standardUserDefaults]valueForKey:@"CustomURL"];
        url=[NSURL URLWithString:strUrl];
        if ([url.scheme isEqualToString:@"http"]||[url.scheme isEqualToString:@"HTTP"]||[url.scheme isEqualToString:@"https"]||[url.scheme isEqualToString:@"HTTPS"])
        {
            NSLog(@"%@ is a valid URL", url);
            return strUrl;
        }
        [alert show];
    }
    return @"";
}*/
-(NSString *)validateUrl2:(NSString *)strUrl
{
    NSString *url=@" ";
    url=[strUrl substringToIndex:4];
    if([url caseInsensitiveCompare:@"http"] == NSOrderedSame)
        return strUrl;
    else{
        strUrl=[[NSUserDefaults standardUserDefaults]valueForKey:@"CustomURL"];
        if(strUrl.length>0)
         url=[strUrl substringToIndex:4];
        
        if([url caseInsensitiveCompare:@"http"] == NSOrderedSame)
        {
            strUrl= [self containSpecialCharacter:strUrl];
             return strUrl;
        }
        [alert show];
    }
    return @"";
}
-(NSString *)containSpecialCharacter:(NSString *)strUrl
{
  
    if (!([strUrl rangeOfString:@"%s"].location==NSNotFound))
    {
        strUrl=[strUrl stringByReplacingOccurrencesOfString:@"%s" withString:[(AVMetadataMachineReadableCodeObject *)metaDataObj stringValue]];
        return strUrl;
        
    }else if(!([strUrl rangeOfString:@"%t"].location==NSNotFound))
    {
        strUrl=[strUrl stringByReplacingOccurrencesOfString:@"%t" withString:[NSString stringWithFormat:@"%@",metaDataObj.type]];
        return strUrl;
    }
    else if(!([strUrl rangeOfString:@"%f"].location==NSNotFound))
    {
        strUrl=[strUrl stringByReplacingOccurrencesOfString:@"%t" withString:[NSString stringWithFormat:@"%@",metaDataObj.type]];
        return strUrl;
    }
    return strUrl;
}

@end
