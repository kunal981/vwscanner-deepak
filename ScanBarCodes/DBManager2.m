//
//  DBManager.m
//  FinalNavitas
//
//  Created by brst on 6/10/14.
//  Copyright (c) 2014 sh. All rights reserved.
//

#import "DBManager2.h"
#import "DBfile.h"

static DBManager2 *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager2

+(DBManager2*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
    
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: @"barcodeDb.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSLog(@"dataBASE PATH=%@",databasePath);
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt ="create table barcode (id integer primary key autoincrement,name text,url text,note text)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table %s",errMsg);
            }
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    NSLog(@"%d",isSuccess);
    return isSuccess;
}

-(BOOL) saveData:(NSString*)name url:(NSString*)url note:(NSString*)note
{
    BOOL success=NO;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@ "insert into barcode (name,url,note) values(\"%@\", \"%@\",\"%@\")",name,url,note];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"insertion Done");
            success= YES;
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    return success;
}

-(BOOL)updateTable:(NSString *)note index:(NSString*)rowID
{
    BOOL success=NO;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@ "update barcode set note = '%@' where id =%d",note,[rowID intValue]];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"UpdateDone Done");
            success= YES;
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    return success;
}



-(BOOL)deleteRecord:(NSString *)barcode_id
{
    BOOL success=NO;
    sqlite3_stmt *statement = NULL;
     const char *dbpath = [databasePath UTF8String];
    NSLog(@"bacode_id=%@",barcode_id);
  
    if(sqlite3_open(dbpath, &database)==SQLITE_OK)
    {
        NSString *query=[NSString stringWithFormat:@"delete from barcode where id=%d",[barcode_id intValue]];
        NSLog(@"delete Query %@",query);
        const char *delete_stmt = [query UTF8String];
        sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success= YES;
        }
        else {
            success= NO;
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(database);
    return success;


}


- (NSMutableArray *) showAll
{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = @"SELECT * FROM barcode order BY id desc";
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                DBfile *dbfile = [[DBfile alloc] init];
                dbfile.barcode_id =[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                dbfile.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                dbfile.barcodeUrl=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                 dbfile.note=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                [list addObject:dbfile];
                dbfile=nil;
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    
    return list;
}

-(BOOL)deleteAll
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"delete from barcode"];
        NSLog(@"delete Query %@",querySQL);
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"%d",sqlite3_step(statement));
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
            return YES;
            
        }
        else{
            sqlite3_close(database);
            return NO;
        }
        
    }
    return NO;
}




@end
