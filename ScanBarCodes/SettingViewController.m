//
//  SettingViewController.m
//  VWScanner
//
//  Created by brst on 3/24/15.
//  Copyright (c) 2015 Infragistics. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    UITableView *table;
    NSUserDefaults *userDeafault;
    UIAlertView *alertView;
    
}

@end

@implementation SettingViewController

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor blackColor]];
    self.view = view;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
  NSString *checkScanStatus=[[NSUserDefaults standardUserDefaults]valueForKey:@"autoScan"];
    NSString *checkNote = [[NSUserDefaults standardUserDefaults]valueForKey:@"Note"];
    if ([checkNote isEqualToString:@"ON"]) {
        selectedRow = 1;
    }
    else if([checkScanStatus isEqualToString:@"ON"]){
        selectedRow = 0 ;
    }
    
    
   // selectedRow = 0;
    [self toplayerView];
    userDeafault=[NSUserDefaults standardUserDefaults];
    [userDeafault synchronize];
    
    table=[[UITableView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
    [table setSeparatorColor:[UIColor grayColor]];
    table.backgroundColor=[UIColor clearColor];
    table.delegate=self;
    table.dataSource=self;
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    table.scrollEnabled=NO;
    [self.view addSubview:table];
    
    alertView=[[UIAlertView alloc]initWithTitle:@"Custom URL" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    alertView.delegate=self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
        
    }
    
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor grayColor];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    if(indexPath.row==0)
    {
        cell.textLabel.text=@"Auto Scan";
        cell.detailTextLabel.text=@"Continuosly scan the Barcodes";
        
        if([[userDeafault valueForKey:@"autoScan"] isEqualToString:@"ON"] && selectedRow == 0)
            cell.accessoryType=UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType=UITableViewCellSelectionStyleNone;
        
    }
    if(indexPath.row==1)
    {
        cell.textLabel.text=@"Notes";
        //cell.detailTextLabel.text=@"Continuosly scan the Barcodes";
        
        if([[userDeafault valueForKey:@"Note"] isEqualToString:@"ON"] && selectedRow ==1)
            cell.accessoryType=UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType=UITableViewCellSelectionStyleNone;
        
    }
    else if(indexPath.row==2){
        cell.textLabel.text=@"Custom search URL";
        cell.detailTextLabel.text=@"Subsitution: %s=contents, %t=type, %f=format";

    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    selectedRow = indexPath.row;
    
    if(indexPath.row==0)
    {
        [userDeafault setObject:@"OFF" forKey:@"Note"];

        if(cell.accessoryType==UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType=UITableViewCellSelectionStyleNone;
            [userDeafault setObject:@"OFF" forKey:@"autoScan"];
        }
        
        
        else
        {
            cell.accessoryType=UITableViewCellAccessoryCheckmark;
            [userDeafault setObject:@"ON" forKey:@"autoScan"];
            
        }
        

    }
    else if(indexPath.row==1)
    {
         [userDeafault setObject:@"OFF" forKey:@"autoScan"];
        if(cell.accessoryType==UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType=UITableViewCellSelectionStyleNone;
            [userDeafault setObject:@"OFF" forKey:@"Note"];
        }
        
        
        else
        {
            cell.accessoryType=UITableViewCellAccessoryCheckmark;
            [userDeafault setObject:@"ON" forKey:@"Note"];
            
        }
        
        
    }
    
    
    else
    {
        UITextField* textField=[alertView textFieldAtIndex:0];
        textField.text=[userDeafault valueForKey:@"CustomURL"];
        [alertView show];
        
    }
    [tableView reloadData];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)toplayerView
{
    UIView *blueLine=[[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 1)];
    blueLine.backgroundColor=[UIColor colorWithRed:0.f green:(153/255.0f) blue:(240/255.0f) alpha:1];
    [self.view addSubview:blueLine];
    
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 25, 25)];
    [backButton setImage:[UIImage imageNamed:@"backSymbol.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UIButton *backButtonSpace=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    
    [backButtonSpace addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonSpace];

    UILabel *VWScannerLabl=[[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 35)];
    VWScannerLabl.text=@"Settings";
    VWScannerLabl.textAlignment=NSTextAlignmentCenter;
    VWScannerLabl.textColor=[UIColor whiteColor];
    [self.view addSubview:VWScannerLabl];
   
}
- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex=%ld",(long)buttonIndex);
    if(buttonIndex==1)
    {
        UITextField* textField=[alertView1 textFieldAtIndex:0];
        [userDeafault setObject:textField.text forKey:@"CustomURL"];
        NSLog(@"%@",textField.text);
        [table reloadData];
    }
    
}
-(void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
